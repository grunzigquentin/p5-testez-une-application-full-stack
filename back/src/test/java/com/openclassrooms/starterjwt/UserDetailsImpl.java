package com.openclassrooms.starterjwt;

import com.openclassrooms.starterjwt.security.services.UserDetailsImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

class UserDetailsImplTest {

    private UserDetailsImpl userDetails;

    @BeforeEach
    void setUp() {
        userDetails = UserDetailsImpl.builder()
                .id(1L)
                .username("test@example.com")
                .firstName("Test")
                .lastName("User")
                .admin(false)
                .password("password")
                .build();
    }

    @Test
    void testGetAuthorities() {
        Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
        assertNotNull(authorities);
        assertTrue(authorities.isEmpty());
    }

    @Test
    void testIsAccountNonExpired() {
        assertTrue(userDetails.isAccountNonExpired());
    }

    @Test
    void testIsAccountNonLocked() {
        assertTrue(userDetails.isAccountNonLocked());
    }

    @Test
    void testIsCredentialsNonExpired() {
        assertTrue(userDetails.isCredentialsNonExpired());
    }

    @Test
    void testIsEnabled() {
        assertTrue(userDetails.isEnabled());
    }

    @Test
    void testEquals_SameObject() {
        assertEquals(userDetails, userDetails);
    }

    @Test
    void testEquals_DifferentObject() {
        UserDetailsImpl otherUserDetails = UserDetailsImpl.builder()
                .id(2L)
                .username("other@example.com")
                .firstName("Other")
                .lastName("User")
                .admin(false)
                .password("password")
                .build();
        assertNotEquals(userDetails, otherUserDetails);
    }

    @Test
    void testEquals_SameIdDifferentObject() {
        UserDetailsImpl otherUserDetails = UserDetailsImpl.builder()
                .id(1L)
                .username("other@example.com")
                .firstName("Other")
                .lastName("User")
                .admin(false)
                .password("password")
                .build();
        assertEquals(userDetails, otherUserDetails);
    }

    @Test
    void testEquals_NullObject() {
        assertNotEquals(userDetails, null);
    }

    @Test
    void testEquals_DifferentClass() {
        assertNotEquals(userDetails, new Object());
    }

    @Test
    void testHashCode_SameObject() {
        assertEquals(userDetails.hashCode(), userDetails.hashCode());
    }

    @Test
    void testHashCode_DifferentObject() {
        UserDetailsImpl otherUserDetails = UserDetailsImpl.builder()
                .id(2L)
                .username("other@example.com")
                .firstName("Other")
                .lastName("User")
                .admin(false)
                .password("password")
                .build();
        assertNotEquals(userDetails.hashCode(), otherUserDetails.hashCode());
    }
}
