package com.openclassrooms.starterjwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.openclassrooms.starterjwt.dto.SessionDto;
import com.openclassrooms.starterjwt.mapper.SessionMapper;
import com.openclassrooms.starterjwt.models.Session;
import com.openclassrooms.starterjwt.services.SessionService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class SessionControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SessionService sessionService;

    @MockBean
    private SessionMapper sessionMapper;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void testFindById_SessionExists() throws Exception {
        Session session = new Session();
        session.setId(1L);
        SessionDto sessionDto = new SessionDto();
        sessionDto.setId(1L);

        Mockito.when(sessionService.getById(anyLong())).thenReturn(session);
        Mockito.when(sessionMapper.toDto(any(Session.class))).thenReturn(sessionDto);

        mockMvc.perform(get("/api/session/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L));
    }

    @Test
    void testFindById_SessionNotFound() throws Exception {
        Mockito.when(sessionService.getById(anyLong())).thenReturn(null);

        mockMvc.perform(get("/api/session/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void testFindAll_SessionsExist() throws Exception {
        Session session = new Session();
        session.setId(1L);
        List<Session> sessions = Collections.singletonList(session);
        SessionDto sessionDto = new SessionDto();
        sessionDto.setId(1L);

        Mockito.when(sessionService.findAll()).thenReturn(sessions);
        Mockito.when(sessionMapper.toDto(any(List.class))).thenReturn(Collections.singletonList(sessionDto));

        mockMvc.perform(get("/api/session"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id").value(1L));
    }

    @Test
    void testFindAll_NoSessions() throws Exception {
        Mockito.when(sessionService.findAll()).thenReturn(Collections.emptyList());

        mockMvc.perform(get("/api/session"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    void testCreateSession_Success() throws Exception {
        SessionDto sessionDto = new SessionDto();
        sessionDto.setName("Updated Session");
        sessionDto.setDate(new Date());
        sessionDto.setTeacher_id(1L);
        sessionDto.setDescription("123");
        Session session = new Session();
        session.setId(1L);
        session.setName("Test Session");

        Mockito.when(sessionService.create(any(Session.class))).thenReturn(session);
        Mockito.when(sessionMapper.toEntity(any(SessionDto.class))).thenReturn(session);
        Mockito.when(sessionMapper.toDto(any(Session.class))).thenReturn(sessionDto);

        mockMvc.perform(post("/api/session")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(sessionDto)))
                .andExpect(status().isOk());
    }

    @Test
    void testUpdateSession_Success() throws Exception {
        SessionDto sessionDto = new SessionDto();
        sessionDto.setName("Updated Session");
        sessionDto.setDate(new Date());
        sessionDto.setTeacher_id(1L);
        sessionDto.setDescription("123");

        Session session = new Session();
        session.setId(1L);
        session.setName("Updated Session");

        Mockito.when(sessionService.update(anyLong(), any(Session.class))).thenReturn(session);
        Mockito.when(sessionMapper.toEntity(any(SessionDto.class))).thenReturn(session);
        Mockito.when(sessionMapper.toDto(any(Session.class))).thenReturn(sessionDto);

        mockMvc.perform(put("/api/session/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(sessionDto)))
                .andExpect(status().isOk());
    }

    @Test
    void testCreate() throws Exception {
        SessionDto sessionDto = new SessionDto();
        sessionDto.setName("Test Session");
        sessionDto.setName("Updated Session");
        sessionDto.setDate(new Date());
        sessionDto.setTeacher_id(1L);
        sessionDto.setDescription("123");
        Session session = new Session();
        session.setId(1L);
        session.setName("Test Session");

        Mockito.when(sessionService.create(any(Session.class))).thenReturn(session);

        mockMvc.perform(post("/api/session")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(sessionDto)))
                .andExpect(status().isOk());
    }

    @Test
    void testUpdate() throws Exception {
        SessionDto sessionDto = new SessionDto();
        sessionDto.setName("Updated Session");
        sessionDto.setDate(new Date());
        sessionDto.setTeacher_id(1L);
        sessionDto.setDescription("123");

        Session session = new Session();
        session.setId(1L);
        session.setName("Updated Session");

        Mockito.when(sessionService.update(anyLong(), any(Session.class))).thenReturn(session);

        mockMvc.perform(put("/api/session/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(sessionDto)))
                .andExpect(status().isOk());
    }

    @Test
    void testSave() throws Exception {
        Session session = new Session(); // Création d'une session factice pour le test
        session.setId(1L);
        Mockito.when(sessionService.getById(1L)).thenReturn(session);

        mockMvc.perform(delete("/api/session/1"))
                .andExpect(status().isOk());
    }
    @Test
    void testSave_SessionNotFound() throws Exception {
        Mockito.when(sessionService.getById(1L)).thenReturn(null);

        mockMvc.perform(delete("/api/session/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void testSave_NumberFormatException() throws Exception {
        mockMvc.perform(delete("/api/session/invalid"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testParticipate() throws Exception {
        Session session = new Session(); // Création d'une session factice pour le test

        mockMvc.perform(post("/api/session/1/participate/1"))
                .andExpect(status().isOk());
    }

    @Test
    void testParticipate_NumberFormatException() throws Exception {
        mockMvc.perform(delete("/api/session/1/participate/invalid"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testNoLongerParticipate() throws Exception {
        Session session = new Session(); // Création d'une session factice pour le test

        mockMvc.perform(delete("/api/session/1/participate/1"))
                .andExpect(status().isOk());
    }
}
