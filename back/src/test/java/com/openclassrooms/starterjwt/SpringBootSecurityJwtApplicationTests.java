package com.openclassrooms.starterjwt;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SpringBootSecurityJwtApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void mainMethodRunsSuccessfully() {
		try {
			SpringBootSecurityJwtApplication.main(new String[] {});
		} catch (Exception e) {
			// Si une exception est levée, le test échoue
			throw new AssertionError("Application failed to start", e);
		}
	}
}
