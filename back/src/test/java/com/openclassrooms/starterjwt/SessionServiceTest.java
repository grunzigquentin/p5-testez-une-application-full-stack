package com.openclassrooms.starterjwt;

import com.openclassrooms.starterjwt.exception.BadRequestException;
import com.openclassrooms.starterjwt.exception.NotFoundException;
import com.openclassrooms.starterjwt.models.Session;
import com.openclassrooms.starterjwt.models.User;
import com.openclassrooms.starterjwt.repository.SessionRepository;
import com.openclassrooms.starterjwt.repository.UserRepository;
import com.openclassrooms.starterjwt.services.SessionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class SessionServiceTest {

    @Mock
    private static SessionRepository sessionRepository;

    @Mock
    private static UserRepository userRepository;

    @InjectMocks
    private SessionService sessionService;
    @Test
    void testCreate() {
        // Initialisation des données de test
        Session session = new Session();
        session.setId(1L);

        when(sessionRepository.save(session)).thenReturn(session);

        // Exécution de la méthode à tester
        Session createdSession = sessionService.create(session);

        // Vérification des résultats
        assertNotNull(createdSession);
        assertEquals(1L, createdSession.getId());
        verify(sessionRepository).save(session);
    }

    @Test
    void testDelete() {
        Long sessionId = 1L;

        // Exécution de la méthode à tester
        sessionService.delete(sessionId);

        // Vérification des résultats
        verify(sessionRepository).deleteById(sessionId);
    }

    @Test
    void testFindAll() {
        // Initialisation des données de test
        Session session1 = new Session();
        session1.setId(1L);

        Session session2 = new Session();
        session2.setId(2L);

        when(sessionRepository.findAll()).thenReturn(List.of(session1, session2));

        // Exécution de la méthode à tester
        List<Session> sessions = sessionService.findAll();

        // Vérification des résultats
        assertNotNull(sessions);
        assertEquals(2, sessions.size());
        verify(sessionRepository).findAll();
    }

    @Test
    void testGetById_Found() {
        // Initialisation des données de test
        Session session = new Session();
        session.setId(1L);

        when(sessionRepository.findById(1L)).thenReturn(Optional.of(session));

        // Exécution de la méthode à tester
        Session foundSession = sessionService.getById(1L);

        // Vérification des résultats
        assertNotNull(foundSession);
        assertEquals(1L, foundSession.getId());
        verify(sessionRepository).findById(1L);
    }

    @Test
    void testGetById_NotFound() {
        when(sessionRepository.findById(1L)).thenReturn(Optional.empty());

        // Exécution de la méthode à tester
        Session foundSession = sessionService.getById(1L);

        // Vérification des résultats
        assertNull(foundSession);
        verify(sessionRepository).findById(1L);
    }

    @Test
    void testUpdate() {
        // Initialisation des données de test
        Session session = new Session();
        session.setId(1L);

        Session updatedSession = new Session();
        updatedSession.setId(1L);
        updatedSession.setName("Updated Session");

        when(sessionRepository.save(session)).thenReturn(updatedSession);

        // Exécution de la méthode à tester
        Session result = sessionService.update(1L, session);

        // Vérification des résultats
        assertNotNull(result);
        assertEquals(1L, result.getId());
        assertEquals("Updated Session", result.getName());
        verify(sessionRepository).save(session);
    }
    @Test
    void testNoLongerParticipateNoSession(){
        when(sessionRepository.findById(1L)).thenReturn(Optional.empty());
        SessionService sessionService = new SessionService(sessionRepository, userRepository);;
        Long SessionId = 1L;
        Long userId = 1L;

        Assertions.assertThrows(NotFoundException.class, () -> sessionService.noLongerParticipate(SessionId, userId));
    }

    @Test
    void testNoLongerParticipateAlreadyNotParticipant(){
        Session session = new Session();
        User user = new User();
        user.setId(1L);
        List<User> listUser = List.of(user);
        session.setUsers(listUser);
        when(sessionRepository.findById(1L)).thenReturn(Optional.of(session));
        SessionService sessionService = new SessionService(sessionRepository, userRepository);
        Long SessionId = 1L;
        Long userId = 2L;

        Assertions.assertThrows(BadRequestException.class, () -> sessionService.noLongerParticipate(SessionId, userId));
    }

    @Test
    void testNoLongerParticipateCorrectSave(){
        Session session = new Session();
        User user = new User();
        user.setId(1L);
        User user2 = new User();
        user2.setId(2L);

        List<User> listUser = List.of(user, user2);

        session.setUsers(listUser);

        Long SessionId = 1L;
        Long userId = 1L;

        Session goodSession = new Session();
        List<User> correctListUser = List.of(user2);
        goodSession.setUsers(correctListUser);

        when(sessionRepository.findById(1L)).thenReturn(Optional.of(session));
        SessionService sessionService = new SessionService(sessionRepository, userRepository);

        ArgumentCaptor<Session> argument = ArgumentCaptor.forClass(Session.class);
        sessionService.noLongerParticipate(SessionId, userId);
        verify(sessionRepository).save(argument.capture());

        assertEquals(argument.getValue().getUsers(), goodSession.getUsers());
    }
    @Test
    void testParticipateUserOrSessionNull(){
        Session session = new Session();
        User user = new User();
        Long NoSessionId = 1L;
        Long NoUserId = 1L;
        Long sessionId = 2L;
        Long userId = 2L;
        when(sessionRepository.findById(1L)).thenReturn(Optional.empty());
        when(userRepository.findById(1L)).thenReturn(Optional.empty());

        when(sessionRepository.findById(2L)).thenReturn(Optional.of(session));
        when(userRepository.findById(2L)).thenReturn(Optional.of(user));

        SessionService sessionServiceNoSession = new SessionService(sessionRepository, userRepository);;

        Assertions.assertThrows(NotFoundException.class, () -> sessionServiceNoSession.participate(NoSessionId, userId));
        Assertions.assertThrows(NotFoundException.class, () -> sessionServiceNoSession.participate(sessionId, NoUserId));
    }
    @Test
    void testParticipateAlreadyParticipating(){
        Session session = new Session();
        User user = new User();
        user.setId(1L);
        Long SessionId = 1L;
        Long userId = 1L;
        List<User> listUser = List.of(user);
        session.setUsers(listUser);

        when(sessionRepository.findById(1L)).thenReturn(Optional.of(session));
        when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        SessionService sessionService = new SessionService(sessionRepository, userRepository);
        Assertions.assertThrows(BadRequestException.class, () -> sessionService.participate(SessionId, userId));
    }

    @Test
    void testParticipateCorrectSave(){
        Session session = new Session();
        Session goodSession = new Session();
        User user = new User();
        User newUser = new User();
        newUser.setId(2L);
        user.setId(1L);

        Long SessionId = 1L;
        Long newUserId = 2L;

        List<User> listUser = List.of(user);
        List<User> correctFinalList = List.of(user, newUser);
        session.setUsers(listUser);
        goodSession.setUsers(correctFinalList);



        when(sessionRepository.findById(1L)).thenReturn(Optional.of(session));
        when(userRepository.findById(2L)).thenReturn(Optional.of(newUser));

        ArgumentCaptor<Session> argument = ArgumentCaptor.forClass(Session.class);
        SessionService sessionService = new SessionService(sessionRepository, userRepository);
        sessionService.participate(SessionId, newUserId);

        verify(sessionRepository).save(argument.capture());
        assertEquals(argument.getValue().getUsers(), goodSession.getUsers());
    }
}