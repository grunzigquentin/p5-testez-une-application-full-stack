package com.openclassrooms.starterjwt;

import com.openclassrooms.starterjwt.models.Teacher;
import com.openclassrooms.starterjwt.repository.TeacherRepository;
import com.openclassrooms.starterjwt.services.TeacherService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class TeacherServiceTest {

    @Mock
    private TeacherRepository teacherRepository;

    @InjectMocks
    private TeacherService teacherService;

    @Test
    void testFindAll() {
        // Initialisation des données de test
        Teacher teacher1 = new Teacher();
        teacher1.setId(1L);

        Teacher teacher2 = new Teacher();
        teacher2.setId(2L);

        when(teacherRepository.findAll()).thenReturn(List.of(teacher1, teacher2));

        // Exécution de la méthode à tester
        List<Teacher> teachers = teacherService.findAll();

        // Vérification des résultats
        assertNotNull(teachers);
        assertEquals(2, teachers.size());
        assertEquals(1L, teachers.get(0).getId());
        assertEquals(2L, teachers.get(1).getId());

        verify(teacherRepository).findAll();
    }

    @Test
    void testFindById_Found() {
        // Initialisation des données de test
        Teacher teacher = new Teacher();
        teacher.setId(1L);

        when(teacherRepository.findById(1L)).thenReturn(Optional.of(teacher));

        // Exécution de la méthode à tester
        Teacher foundTeacher = teacherService.findById(1L);

        // Vérification des résultats
        assertNotNull(foundTeacher);
        assertEquals(1L, foundTeacher.getId());

        verify(teacherRepository).findById(1L);
    }

    @Test
    void testFindById_NotFound() {
        when(teacherRepository.findById(1L)).thenReturn(Optional.empty());

        // Exécution de la méthode à tester
        Teacher foundTeacher = teacherService.findById(1L);

        // Vérification des résultats
        assertNull(foundTeacher);

        verify(teacherRepository).findById(1L);
    }
}