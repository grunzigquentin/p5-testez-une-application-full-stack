package com.openclassrooms.starterjwt;

import com.openclassrooms.starterjwt.models.User;
import com.openclassrooms.starterjwt.repository.UserRepository;
import com.openclassrooms.starterjwt.services.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
public class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @Test
    void testDelete() {
        Long userId = 1L;

        // Exécution de la méthode à tester
        userService.delete(userId);

        // Vérification des résultats
        verify(userRepository).deleteById(userId);
    }

    @Test
    void testFindById_Found() {
        // Initialisation des données de test
        User user = new User();
        user.setId(1L);

        when(userRepository.findById(1L)).thenReturn(Optional.of(user));

        // Exécution de la méthode à tester
        User foundUser = userService.findById(1L);

        // Vérification des résultats
        assertNotNull(foundUser);
        assertEquals(1L, foundUser.getId());

        verify(userRepository).findById(1L);
    }

    @Test
    void testFindById_NotFound() {
        when(userRepository.findById(1L)).thenReturn(Optional.empty());

        // Exécution de la méthode à tester
        User foundUser = userService.findById(1L);

        // Vérification des résultats
        assertNull(foundUser);

        verify(userRepository).findById(1L);
    }
}
