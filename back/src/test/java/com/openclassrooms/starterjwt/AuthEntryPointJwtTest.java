package com.openclassrooms.starterjwt;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.openclassrooms.starterjwt.security.jwt.AuthEntryPointJwt;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class AuthEntryPointJwtTest {

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private AuthenticationException authException;

    @InjectMocks
    private AuthEntryPointJwt authEntryPointJwt;

    private ByteArrayOutputStream outputStream;
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() throws IOException {
        MockitoAnnotations.openMocks(this);
        authEntryPointJwt = new AuthEntryPointJwt();
        outputStream = new ByteArrayOutputStream();
        objectMapper = new ObjectMapper();

        // Mock behavior for response.getOutputStream()
        when(response.getOutputStream()).thenReturn(new StubServletOutputStream(outputStream));
    }

    @Test
    public void testCommence() throws IOException, ServletException {
        // Mock HttpServletRequest
        when(request.getServletPath()).thenReturn("/api/test");

        // Mock AuthenticationException
        when(authException.getMessage()).thenReturn("Unauthorized access");

        // Invoke commence method
        authEntryPointJwt.commence(request, response, authException);

        // Verify response details
        verify(response).setContentType(MediaType.APPLICATION_JSON_VALUE);
        verify(response).setStatus(HttpServletResponse.SC_UNAUTHORIZED);

        // Verify response body
        String responseBody = outputStream.toString();
        Map<String, Object> responseMap = objectMapper.readValue(responseBody, Map.class);

        assertEquals(HttpServletResponse.SC_UNAUTHORIZED, responseMap.get("status"));
        assertEquals("Unauthorized", responseMap.get("error"));
        assertEquals("Unauthorized access", responseMap.get("message"));
        assertEquals("/api/test", responseMap.get("path"));
    }

    // StubServletOutputStream to mock HttpServletResponse.getOutputStream()
    private static class StubServletOutputStream extends ServletOutputStream {
        private final ByteArrayOutputStream outputStream;

        public StubServletOutputStream(ByteArrayOutputStream outputStream) {
            this.outputStream = outputStream;
        }

        @Override
        public void write(int b) throws IOException {
            outputStream.write(b);
        }

        @Override
        public boolean isReady() {
            return false;
        }

        @Override
        public void setWriteListener(WriteListener writeListener) {

        }
    }
}





