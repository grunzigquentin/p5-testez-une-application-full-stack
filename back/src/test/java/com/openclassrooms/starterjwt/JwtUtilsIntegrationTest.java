package com.openclassrooms.starterjwt;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import com.openclassrooms.starterjwt.security.jwt.JwtUtils;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.test.context.ActiveProfiles;

import com.openclassrooms.starterjwt.security.services.UserDetailsImpl;

@SpringBootTest
@ActiveProfiles("test")
class JwtUtilsIntegrationTest {

    @Autowired
    private JwtUtils jwtUtils;

    private Authentication authentication;

    @BeforeEach
    void setUp() {
        UserDetailsImpl userDetails = UserDetailsImpl.builder()
                .id(1L)
                .username("testuser@example.com")
                .password("password")
                .build();
        authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
    }

    @Test
    void testGenerateJwtToken() {
        String token = jwtUtils.generateJwtToken(authentication);
        assertNotNull(token);
    }

    @Test
    void testGetUserNameFromJwtToken() {
        String token = jwtUtils.generateJwtToken(authentication);
        String username = jwtUtils.getUserNameFromJwtToken(token);
        assertEquals("testuser@example.com", username);
    }

    @Test
    void testValidateJwtToken_ValidToken() {
        String token = jwtUtils.generateJwtToken(authentication);
        assertTrue(jwtUtils.validateJwtToken(token));
    }

    @Test
    void testValidateJwtToken_InvalidToken() {
        String invalidToken = "invalidToken";
        assertFalse(jwtUtils.validateJwtToken(invalidToken));
    }

    @Test
    void testValidateJwtToken_ExpiredToken() throws InterruptedException {
        // Simulate expiration by setting a short expiration time
        String token = Jwts.builder()
                .setSubject("testuser@example.com")
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime() + 1000)) // 1 second expiration
                .signWith(SignatureAlgorithm.HS512, "testSecretKey")
                .compact();

        Thread.sleep(2000); // Wait for 2 seconds to ensure the token is expired

        assertFalse(jwtUtils.validateJwtToken(token));
    }
}
