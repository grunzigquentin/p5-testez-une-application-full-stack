describe('End to End', () => {
  describe('get personal data and log', () => {
    it('Login successfull', () => {
      cy.visit('/login');

      cy.get('input[formControlName=email]').type('yoga@studio.com');
      cy.get('input[formControlName=password]').type('test!1234');

      cy.get('.submit').click();
      cy.url().should('include', '/sessions');

      cy.contains('Account').click();
      cy.contains('You are admin');
      cy.contains('Sessions').click();
    });
  });

  describe('create a rentals', () => {
    it('go on the rentals page', () => {
      cy.get('.create').click();
      cy.url().should('include', '/create');
    });

    it('create a rentals', () => {
      cy.get('input[formControlName=name]').type('Test');
      cy.get('input[formControlName=date]').type('1999-12-31');
      cy.get('mat-select[formControlName=teacher_id]').click();
      cy.contains('Margot').click();
      cy.get('textarea').type('Une description');
      cy.contains('Save').click();
      cy.contains('Session created');
    });
  });

  describe('Get, delete and edit one rental', () => {
    it('edit the rental', () => {
      cy.contains('Edit').first().click();
      cy.get('input[formControlName=name]').clear().type('Test2');
      cy.contains('Save').click();
      cy.contains('Session updated !');
    });

    it('get one rental', () => {
      cy.contains('Detail').first().click();

      cy.url().should('include', '/detail');
      cy.get('h1').contains('Test');
    });

    it('delete the rental', () => {
      cy.contains('delete').click();
      cy.contains('Session deleted !');
    });
  });

  describe('Register', () => {
    it('Register a user', () => {
      cy.visit('/register');

      cy.get('input[formControlName=firstName]').type('registerUser');
      cy.get('input[formControlName=lastName]').type('registerUser');
      cy.get('input[formControlName=email]').type('test@gmail.com');
      cy.get('input[formControlName=password]').type('test!1234');

      cy.contains('Submit').click();
    });

    it('Connect with this user', () => {
      cy.visit('/login');

      cy.get('input[formControlName=email]').type('test@gmail.com');
      cy.get('input[formControlName=password]').type('test!1234');

      cy.get('.submit').click();
    });

    it('Delete this user', () => {
      cy.contains('Account').click();

      cy.contains('Detail').click();

      cy.contains('Your account has been deleted');
    });
  });
});
