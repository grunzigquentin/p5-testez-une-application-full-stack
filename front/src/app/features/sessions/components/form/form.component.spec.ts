import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import {  ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { expect } from '@jest/globals';
import { SessionService } from 'src/app/services/session.service';
import { SessionApiService } from '../../services/session-api.service';

import { FormComponent } from './form.component';

describe('FormComponent', () => {
  let component: FormComponent;
  let fixture: ComponentFixture<FormComponent>;

  const mockSessionService = {
    sessionInformation: {
      admin: true
    }
  } 

  beforeEach(async () => {
    await TestBed.configureTestingModule({

      imports: [
        RouterTestingModule,
        HttpClientModule,
        MatCardModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        ReactiveFormsModule, 
        MatSnackBarModule,
        MatSelectModule,
        BrowserAnimationsModule
      ],
      providers: [
        { provide: SessionService, useValue: mockSessionService },
        SessionApiService
      ],
      declarations: [FormComponent]
    })
      .compileComponents();

    fixture = TestBed.createComponent(FormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  const validName = "test"
  const validDate = "test"
  const validDescription = "Test"
  const moreThanTwoThousandString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sapien dui, dapibus ut elit tincidunt, fermentum pretium ligula. Aliquam pulvinar odio et orci pellentesque, at egestas lorem condimentum. Aliquam interdum bibendum sem, a hendrerit dui aliquam eu. Sed aliquam, orci pharetra auctor ultrices, lacus nisl rhoncus urna, eget fringilla urna nisi laoreet dui. Praesent quis elit vel dolor elementum efficitur imperdiet vel mauris. Morbi sodales ullamcorper neque eu luctus. Cras fringilla tempus mauris id dignissim. Donec luctus quis eros et ullamcorper. Donec maximus, libero sit amet venenatis malesuada, purus dolor fringilla metus, ac sollicitudin metus nibh vel erat. Ut ac convallis odio.Vestibulum accumsan tellus non quam interdum laoreet. Proin in suscipit enim. In hac habitasse platea dictumst. In et tristique turpis, non finibus dui. Etiam interdum a augue vitae rhoncus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Quisque molestie, elit sed aliquet molestie, velit orci porta ex, in laoreet velit felis vel tortor. Duis sagittis ante nulla, vitae convallis elit sagittis vel. Vestibulum vehicula venenatis vehicula. Integer ut dignissim orci. Aenean eu consectetur ante.Donec blandit mollis velit, euismod ullamcorper enim rhoncus et. Sed eu neque euismod, tincidunt libero eget, varius sem. Aenean eros sem, lacinia vel consectetur elementum, luctus id mauris. Vivamus eu purus eros. Ut consequat ligula augue. Sed suscipit euismod sapien, eu venenatis turpis gravida eget. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Nullam tincidunt feugiat nibh et rutrum. In a tempus metus, in suscipit erat. Pellentesque non euismod dui. Duis luctus purus non nisi bibendum, vitae convallis sapien mollis.Duis euismod ac risus id fermentum. Nullam dapibus interdum dolor in viverra. Quisque commodo nulla id tellus porttitor mi."
  const validID = "1"
  const noData = ""

  function setCorrectValue(){
    component.sessionForm!.get('name')!.setValue(validName)
    component.sessionForm!.get('date')!.setValue(validDate)
    component.sessionForm!.get('teacher_id')!.setValue(validID)
    component.sessionForm!.get('description')!.setValue(validDescription)
  }
  it('should create', () => {
    expect(component).toBeTruthy();
  });
  describe("Login form validation", () => {

    it("should return true because all the field provided are those expected", () => {
      setCorrectValue();
      component.sessionForm!.updateValueAndValidity
      expect(component.sessionForm!.valid).toBe(true)
      })

      describe("name validation", () => {
        it("should return false because name is empty", () => {
          setCorrectValue();
          component.sessionForm!.get('name')!.setValue(noData)
          component.sessionForm!.updateValueAndValidity
          expect(component.sessionForm!.valid).toBe(false)
          })
      })

      describe("Date validation", () => {
        it("should return false because Date is empty", () => {
          setCorrectValue();
          component.sessionForm!.get('date')!.setValue(noData)
          component.sessionForm!.updateValueAndValidity
          expect(component.sessionForm!.valid).toBe(false)
          })
      })

      describe("Description validation", () => {
        it("should return false because Description is empty", () => {
          setCorrectValue();
          component.sessionForm!.get('description')!.setValue(noData)
          component.sessionForm!.updateValueAndValidity
          expect(component.sessionForm!.valid).toBe(false)
          })

          it("should return false because Description is to large", () => {
            setCorrectValue();
            component.sessionForm!.get('description')!.setValue(moreThanTwoThousandString)
            component.sessionForm!.updateValueAndValidity
            expect(component.sessionForm!.valid).toBe(false)
          })
      })

      describe("Teacher_id validation", () => {
        it("should return false because teacher_id is empty", () => {
          setCorrectValue();
          component.sessionForm!.get('teacher_id')!.setValue(noData)
          component.sessionForm!.updateValueAndValidity
          expect(component.sessionForm!.valid).toBe(false)
          })
      })

      it("should return true because all the field provided are those expected", () => {
        setCorrectValue();
        component.sessionForm!.updateValueAndValidity
        expect(component.sessionForm!.valid).toBe(true)
        })
    })
})
