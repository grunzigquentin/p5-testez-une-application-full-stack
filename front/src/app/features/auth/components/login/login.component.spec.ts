import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { LoginComponent } from './login.component';
import { AuthService } from '../../services/auth.service';
import { SessionService } from 'src/app/services/session.service';
import { SessionInformation } from 'src/app/interfaces/sessionInformation.interface';
import { expect } from '@jest/globals';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authService: jest.Mocked<AuthService>;
  let router: Router;
  let sessionService: jest.Mocked<SessionService>;

  beforeEach(async () => {
    const authServiceMock = {
      login: jest.fn(),
    };

    const sessionServiceMock = {
      logIn: jest.fn(),
    };

    await TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [
        ReactiveFormsModule,
        MatIconModule,
        MatFormFieldModule,
        MatInputModule,
        MatCardModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: AuthService, useValue: authServiceMock },
        { provide: Router, useValue: { navigate: jest.fn() } },
        { provide: SessionService, useValue: sessionServiceMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    authService = TestBed.inject(AuthService) as jest.Mocked<AuthService>;
    router = TestBed.inject(Router);
    sessionService = TestBed.inject(
      SessionService
    ) as jest.Mocked<SessionService>;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a valid form when valid input is provided', () => {
    component.form.controls['email'].setValue('test@example.com');
    component.form.controls['password'].setValue('password');
    expect(component.form.valid).toBe(true);
  });

  it('should have an invalid form when invalid input is provided', () => {
    component.form.controls['email'].setValue('');
    component.form.controls['password'].setValue('');
    expect(component.form.valid).toBe(false);
  });

  it('should call authService.login and navigate on successful login', () => {
    const sessionInfo: SessionInformation = {
      id: 1,
      token: '',
      type: '',
      username: '',
      firstName: '',
      lastName: '',
      admin: false,
    };
    authService.login.mockReturnValue(of(sessionInfo));

    component.form.controls['email'].setValue('test@example.com');
    component.form.controls['password'].setValue('password');

    component.submit();

    expect(authService.login).toHaveBeenCalledWith({
      email: 'test@example.com',
      password: 'password',
    });
    expect(sessionService.logIn).toHaveBeenCalledWith(sessionInfo);
    expect(router.navigate).toHaveBeenCalledWith(['/sessions']);
  });

  it('should set onError to true on login error', () => {
    authService.login.mockReturnValue(
      throwError(() => new Error('Login error'))
    );

    component.form.controls['email'].setValue('test@example.com');
    component.form.controls['password'].setValue('password');

    component.submit();

    expect(authService.login).toHaveBeenCalledWith({
      email: 'test@example.com',
      password: 'password',
    });
    expect(component.onError).toBe(true);
  });
});
