import { TestBed, inject } from '@angular/core/testing';
import { SessionService } from './session.service';
import { BehaviorSubject } from 'rxjs';
import { SessionInformation } from '../interfaces/sessionInformation.interface';
import { expect } from '@jest/globals';
describe('SessionService', () => {
  let service: SessionService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SessionService],
    });
    service = TestBed.inject(SessionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should log in a user', () => {
    const user: SessionInformation = {
      token: '',
      type: '',
      id: 0,
      username: '',
      firstName: '',
      lastName: '',
      admin: false,
    };
    service.logIn(user);

    expect(service.isLogged).toBeTruthy();
    expect(service.sessionInformation).toEqual(user);
  });

  it('should log out a user', () => {
    service.logIn({
      token: '',
      type: '',
      id: 0,
      username: '',
      firstName: '',
      lastName: '',
      admin: false,
    });
    service.logOut();

    expect(service.isLogged).toBeFalsy();
    expect(service.sessionInformation).toBeUndefined();
  });

  it('should emit isLoggedSubject correctly after login and logout', () => {
    let isLogged: boolean | undefined;
    service.$isLogged().subscribe((logged) => (isLogged = logged));

    service.logIn({
      token: '',
      type: '',
      id: 0,
      username: '',
      firstName: '',
      lastName: '',
      admin: false,
    });
    expect(isLogged).toBeTruthy();

    service.logOut();
    expect(isLogged).toBeFalsy();
  });
});
