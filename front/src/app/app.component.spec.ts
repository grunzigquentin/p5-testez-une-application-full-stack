import { TestBed, ComponentFixture } from '@angular/core/testing';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { AppComponent } from './app.component';
import { AuthService } from './features/auth/services/auth.service';
import { SessionService } from './services/session.service';
import { RouterTestingModule } from '@angular/router/testing';
import { expect } from '@jest/globals';
import { MatToolbarModule } from '@angular/material/toolbar';
describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let sessionService: jest.Mocked<SessionService>;
  let router: Router;

  beforeEach(async () => {
    const sessionServiceMock = {
      logOut: jest.fn(),
      $isLogged: jest.fn(),
    };

    await TestBed.configureTestingModule({
      declarations: [AppComponent],
      imports: [RouterTestingModule, MatToolbarModule],
      providers: [
        { provide: AuthService, useValue: {} },
        { provide: SessionService, useValue: sessionServiceMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    sessionService = TestBed.inject(
      SessionService
    ) as jest.Mocked<SessionService>;
    router = TestBed.inject(Router);
  });

  it('should create the app', () => {
    expect(component).toBeTruthy();
  });

  it('should return isLogged observable from sessionService', () => {
    const isLogged$ = of(true);
    sessionService.$isLogged.mockReturnValue(isLogged$);

    component.$isLogged().subscribe((isLogged) => {
      expect(isLogged).toBe(true);
    });

    expect(sessionService.$isLogged).toHaveBeenCalled();
  });

  it('should call logout on sessionService and navigate to home', () => {
    const navigateSpy = jest.spyOn(router, 'navigate');
    fixture.ngZone?.run(() => {
      component.logout();
    });

    expect(sessionService.logOut).toHaveBeenCalled();
    expect(navigateSpy).toHaveBeenCalledWith(['']);
  });
});
