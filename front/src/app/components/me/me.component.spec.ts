import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { MeComponent } from './me.component';
import { SessionService } from '../../services/session.service';
import { UserService } from '../../services/user.service';
import { User } from '../../interfaces/user.interface';
import { expect } from '@jest/globals';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
describe('MeComponent', () => {
  let component: MeComponent;
  let fixture: ComponentFixture<MeComponent>;
  let router: Router;
  let sessionService: jest.Mocked<SessionService>;
  let matSnackBar: jest.Mocked<MatSnackBar>;
  let userService: jest.Mocked<UserService>;

  beforeEach(async () => {
    const sessionServiceMock = {
      sessionInformation: { id: 1 },
      logOut: jest.fn(),
    };

    const matSnackBarMock = {
      open: jest.fn(),
    };

    const userServiceMock = {
      getById: jest.fn(),
      delete: jest.fn(),
    };

    await TestBed.configureTestingModule({
      declarations: [MeComponent],
      imports: [MatCardModule, MatIconModule],
      providers: [
        { provide: Router, useValue: { navigate: jest.fn() } },
        { provide: SessionService, useValue: sessionServiceMock },
        { provide: MatSnackBar, useValue: matSnackBarMock },
        { provide: UserService, useValue: userServiceMock },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(MeComponent);
    component = fixture.componentInstance;
    router = TestBed.inject(Router);
    sessionService = TestBed.inject(
      SessionService
    ) as jest.Mocked<SessionService>;
    matSnackBar = TestBed.inject(MatSnackBar) as jest.Mocked<MatSnackBar>;
    userService = TestBed.inject(UserService) as jest.Mocked<UserService>;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch user on init', () => {
    const user: User = {
      id: 1,
      email: 'john.doe@example.com',
      lastName: '',
      firstName: '',
      admin: false,
      password: '',
      createdAt: new Date(),
    };
    userService.getById.mockReturnValue(of(user));

    component.ngOnInit();

    expect(userService.getById).toHaveBeenCalledWith('1');
    expect(component.user).toEqual(user);
  });

  it('should navigate back on back()', () => {
    const backSpy = jest.spyOn(window.history, 'back');
    component.back();
    expect(backSpy).toHaveBeenCalled();
  });

  it('should delete user and navigate to home on delete()', () => {
    userService.delete.mockReturnValue(of({}));
    const navigateSpy = jest.spyOn(router, 'navigate');

    component.delete();

    expect(userService.delete).toHaveBeenCalledWith('1');
    expect(matSnackBar.open).toHaveBeenCalledWith(
      'Your account has been deleted !',
      'Close',
      { duration: 3000 }
    );
    expect(sessionService.logOut).toHaveBeenCalled();
    expect(navigateSpy).toHaveBeenCalledWith(['/']);
  });
});
